Fichiers communs pour mes (anciens) cahiers de textes et [mon site web](http://ababsurdo.fr), tournant sous [IkiWiki](http://ikiwiki.info).

Ces fichiers sont publiées sous les licences suivantes, au choix :

* [Gnu GPL](https://www.gnu.org/licenses/gpl.html), version 2 ou supérieure ;
* [Gnu AGPL](https://www.gnu.org/licenses/agpl.html), version 2 ou supérieure ;
* [Do What The Fuck You Want To Public License](http://www.wtfpl.net/) ou sa traduction française la [Licence Publique Rien À Branler](http://sam.zoy.org/lprab/) ;
* [CC0 1.0 universel](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) ;
* versé dans le domaine public, dans les législations qui le permettent (ce qui n'est pas le cas de la France).
